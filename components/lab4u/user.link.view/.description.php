<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Localization\Loc;

$arComponentDescription = [
    "NAME" => Loc::getMessage("LAB4U_USERLINKVIEW_COMPONENT"),
    "DESCRIPTION" => Loc::getMessage("LAB4U_USERLINKVIEW_COMPONENT_DESCRIPTION"),
    "COMPLEX" => "N",
    "PATH" => [
        "ID" => Loc::getMessage("LAB4U_USERLINKVIEW_COMPONENT_PATH_ID"),
        "NAME" => Loc::getMessage("LAB4U_USERLINKVIEW_COMPONENT_PATH_NAME"),
        "CHILD" => [
            "ID" => Loc::getMessage("LAB4U_USERLINKVIEW_COMPONENT_CHILD_PATH_ID"),
            "NAME" => GetMessage("LAB4U_USERLINKVIEW")
        ]
    ],
];
?>