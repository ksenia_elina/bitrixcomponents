<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$MESS["EXAMPLE_USERLINKADD_PROP_SETTINGS"] = "Выбор инфоблока и разделов";
$MESS["EXAMPLE_USERLINKADD_PROP_IBLOCK_TYPE"] = "Тип инфоблока";
$MESS["EXAMPLE_USERLINKADD_PROP_IBLOCK_ID"] = "Инфоблок";
$MESS["EXAMPLE_USERLINKADD_PROP_SECTION_IDS"] = "ID разделов через запятую";