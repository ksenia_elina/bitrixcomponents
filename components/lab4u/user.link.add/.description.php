<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Localization\Loc;

$arComponentDescription = [
    "NAME" => Loc::getMessage("LAB4U_USERLINKADD_COMPONENT"),
    "DESCRIPTION" => Loc::getMessage("LAB4U_USERLINKADD_COMPONENT_DESCRIPTION"),
    "COMPLEX" => "N",
    "PATH" => [
        "ID" => Loc::getMessage("LAB4U_USERLINKADD_COMPONENT_PATH_ID"),
        "NAME" => Loc::getMessage("LAB4U_USERLINKADD_COMPONENT_PATH_NAME"),
    ],
];
?>

